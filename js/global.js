$(document).ready(function() {
    var bucleIE;
    var flagHorizontal = true;
    var flagVertical = 0;
    var flagDisparo = true;
    var divsIE;
    //var divsIE = $(".divRow");
    bucleIE = setInterval(function() {
        divsIE = $(".divRow");
        if (flagHorizontal) {
            if (divsIE.length == 3) {
                $(divsIE.siblings()[0]).animate({marginLeft: '6%'}, 800);
                $(divsIE.siblings()[1]).animate({marginLeft: '-6%'}, 800);
                $(divsIE.siblings()[2]).animate({marginLeft: '6%'}, 800);

            } else if (divsIE.length == 2) {
                $(divsIE.siblings()[0]).animate({marginLeft: '6%'}, 800);
                $(divsIE.siblings()[1]).animate({marginLeft: '-6%'}, 800);


            } else {
                $(divsIE).animate({marginLeft: '6%'}, 800);


            }
            flagHorizontal = false;
            flagVertical++;

        } else {

            if (divsIE.length == 3) {
                $(divsIE.siblings()[0]).animate({marginLeft: '-6%'}, 800);
                $(divsIE.siblings()[1]).animate({marginLeft: '6%'}, 800);
                $(divsIE.siblings()[2]).animate({marginLeft: '-6%'}, 800);

            } else if (divsIE.length == 2) {
                $(divsIE.siblings()[0]).animate({marginLeft: '-6%'}, 800);
                $(divsIE.siblings()[1]).animate({marginLeft: '6%'}, 800);

            } else {
                $(divsIE).animate({marginLeft: '-6%'}, 800);

            }
            flagHorizontal = true;
            flagVertical++;


        }
        if (flagVertical % 2 == 0) {
            $("#contRow").css({"marginTop": '+=15'});
        }


        var colliders_IE = ".divrow ul";
        var obstacles_Client = "#divCliente";
        var hitsIE_Cli = $(colliders_IE).collision(obstacles_Client);
        console.log(hitsIE_Cli.length);
        if (hitsIE_Cli.length != 0) {
            $(divsIE).stop();
            clearInterval(bucleIE);
            alert("Game Over!");
        }


    }, 1000);
    $(document).on('keydown', function(e) {




        if (e.which == 37 && parseFloat($("#divProgrammer").css("left")) >= "9") {
            $("#divProgrammer").stop(true, true);
            $("#divProgrammer").animate({"left": "-=10"}, "fast");
        } else if (e.which == 39 && parseFloat($("#divProgrammer").css("left")) <= "639") {
            $("#divProgrammer").stop(true, true);
            $("#divProgrammer").animate({"left": "+=10"}, "fast");
        }


        if (e.which == 38) {
            if (flagDisparo) {
                flagDisparo = false;
                $("#divDisparo").animate({"top": "-606px"}, {
                    step: function() {
                        var colliders_IEli = $(".divrow ul li");
                        var colliders_Disparo = $("#divDisparo");
                        var hitsIE_Disp = $(colliders_Disparo).collision(colliders_IEli);
                        if (hitsIE_Disp.length != 0) {

                            $("#divDisparo").stop();

                            $("#divDisparo").fadeOut("fast", function() {
                                $(this).css({"top": "0px", "display": "inline"});
                                flagDisparo = true;
                            });
                            $(hitsIE_Disp).remove();
                        }
                        divsIE = $(".divRow");
                        divsIE.each(function(x) {
                            if ($(this).find("li").length == 0) {
                                $(this).remove();
                                if ($(".divRow").length == 0) {
                                    clearInterval(bucleIE);

                                    alert("Win! El cliente dice que no usa IE :P");
                                }
                            }
                        });

                    },
                    complete: function() {
                        $("#divDisparo").fadeOut("fast", function() {
                            $(this).css({"top": "0px", "display": "inline"});
                            flagDisparo = true;
                        });
                    }
                });
            }
        }

    });
});